export default {
    NORMAL: 'normal',
    MILITARY: 'military',
    ENTREPRENEUR: 'entrepreneur',
    JUDGE: 'judge',
    JOURNALIST: 'journalist',
    DEPUTY: 'deputy',
    SCIENTIST: 'scientist',
    MINER: 'miner',
    GOVERNMENT: 'government'
}