jQuery(document).ready(function() {

    var p = {};
    p.type = "normal";

    $(".slider").each(function() {
        
        var a = $(this);
        a.slider({
            min: a.data("min") || 0,
            max: a.data("max") || 1e4,
            value: a.data("default") || a.data("min") || 0,
            step: a.data("step") || 1,
            slide: function(e, r) {
                $("#" + a.data("for")).val(r.value).trigger("change")
            }
        }), $("#" + a.data("for")).val(a.slider("value")).change(function() {
            var e = $(this);
            e.val(parseInt(e.val()) || 0), a.slider("value", e.val())
        })

    }); 

    $('input[type=radio][name=type]').change(function() { 
        $('[class^=for-type-]').addClass('hidden');
          $('.for-type-'+this.value).toggleClass('hidden');
          p.type = $("input[name=type]:radio:checked").val();
    });

    $('#experience').change(function() {
        if (this.value < $('#experience-'+p.type).val()) {
          var e = $('#experience-'+p.type);
          e.val(parseInt($("#experience").val())); 
          e.slider().change();          
        }

    });
    
    $('[id^=experience-]').change(function() {
        if (this.value > $('#experience').val()) {
          var a = $('#experience-'+p.type);
          a.val(parseInt($("#experience").val()));         
        }
    });

    var currentWidth = window.innerWidth || document.documentElement.clientWidth;
    var top = $(window).scrollTop();
    var to_result = $('#result').offset().top - $(window).height();
    var to_dodat =  $('#dodatBlock').offset().top + $('#dodatBlock').height();

    $('#calculator input').change(function() {

      if (currentWidth >= 768) {
        if ($(window).scrollTop() < to_result ) {
              
              if (!$('#result').hasClass('fix_table')) {
                t_width = $('#result').width();
                $('#result').css({"bottom": "0px", "width": t_width}).addClass('fix_table').addClass('wfixed');
              }
              else {
                $('#result').css({"width": t_width});
              }
        }
      }

    });

    function scrl() {
      if  ($(window).scrollTop() >= $('#footer').offset().top - $(window).height()) 
      {
        if ( $('#result').hasClass('fix_table')) {     
          $('#result').removeClass('fix_table');
        }
      }
      if ($(window).scrollTop() < $('#footer').offset().top - $(window).height())
      { 
        if ( $('#result').hasClass('wfixed')) {      
          t_width = $('#result').width();
          $('#result').css({"width": t_width}).addClass('fix_table');
        }
      }
      
    };

    if (currentWidth >= 768) {
      $(window).scroll(scrl);
    }

  	jQuery('#year-of-birth-list > li').click(function(e){
  	  e.preventDefault();
  	  var selected = jQuery(this).text();
  	  jQuery('#year-of-birth').text(selected);

      if (currentWidth >= 768) {
        if ($(window).scrollTop() < to_result ) {
              
              if (!$('#result').hasClass('fix_table')) {
                t_width = $('#result').width();
                $('#result').css({"bottom": "0px", "width": t_width}).addClass('fix_table').addClass('wfixed');
              }
              else {
                $('#result').css({"width": t_width});
              }
        }      
      }
  	});

    jQuery("[data-toggle=popover]").popover();
	
});